/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forthdayabstct;

import java.util.Scanner;

/**
 *
 * @author SURAJ BANIYA
 */
public class Student3 implements Student2{

    @Override
    public int readInt() {
         return (new Scanner(System.in).nextInt());
    }

    @Override
    public double readDouble() {
        return (new Scanner(System.in).nextDouble());
    }

    @Override
    public String readString() {
        return (new Scanner(System.in).nextLine());
    }

    @Override
    public void printValues(String label, String value) {
        System.out.println(label +":"+ value);
    }
    
}
