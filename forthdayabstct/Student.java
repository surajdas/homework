 
package forthdayabstct;

public class Student implements Student1{
    
    private String name;
    private String address;
    private int rollnum;
    private double m1;
    private double m2;
    private double m3;
    private double m4;
    private double total;
    private double averg;
    private  String result;
    private String division;
    public Student(){
        this.name="";
        this.address="";
        this.rollnum=0;
        this.m1=0;
        this.m2=0;
        this.m3=0;
        this.m4=0;
        this.total=0;
        this.averg=0;
        this.result="";
        this.division="";
        
    }
    public Student(String name,String address,int id,double m1,double m2,double m3,double m4 ){
        this.name=name;
        this.address=address;
        this.rollnum=id;
        this.m1=m1;
        this.m2=m2;
        this.m3=m3;
        this.m4=m4;
        this.total=total;
        this.result=result;
    }
    
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return this.name;
    }
    public void setAddress(String address){
        this.address=address;
        
        
    }
   public String getAddress(){
        return this.address;
    }
    public void setRollNum(int id){
        this.rollnum=id;
    }
    public int getRollNum(){
       return this.rollnum;
    }
    public void setM1(double m1){
        this.m1=m1;
    }
    public double getM1(){
        return this.m1;
    }
    public void SetM2(double m2){
        this.m2=m2;
        
    }
    public double getM2(){
        return this.m2;
    }
    public void setM3(double m3){
        this.m3=m3;
    }
    public double getM3()
    {
       return  this.m3;
    }
    public void setM4(double m4){
        this.m4=m4;
    }
    public double getM4(){
        return this.m4;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", address=" + address + ", rollnum=" + rollnum + ", m1=" + m1 + ", m2=" + m2 + ", m3=" + m3 + ", m4=" + m4 + ", total=" + total + ", averg=" + averg + ", result=" + result + ", division=" + division + '}';
    }
 
     

    @Override
    public void calcTotal() {
        this.total=this.m1+this.m2+this.m3+this.m4;
    }

    @Override
    public void CalcAverage() {
        
        this.averg=this.total/MAX_SIZE;
        
    }
    
      @Override
    public void calResult() {
        if(this.m1>=PAS && this.m2>=PAS && this.m3>=PAS && this.m4>=PAS )
            this.result="PASS";
        else
            this.result="FAIL";
    }

    @Override
    public void calcDivision() {
        if(this.result.equals("PASS") && this.averg>=DIS)
        {
            this.division="DISTINCTION";
        }
        else if(this.result.equals("PASS") && this.averg<DIS && this.averg>=FIR)
        {
           this.division="FIRST DIVISION";
        }
        else if(this.result.equals("PASS") && this.averg<FIR && this.averg>=SEC)
        {
            this.division="SECOND DIISION";
        }
        else
        {
            this.division="FAIL";
        }
    }

   
}