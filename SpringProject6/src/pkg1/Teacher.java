package pkg1;

public class Teacher {
	private String name;
	private String address;
	private String email;
	
	
	public Teacher() {
		super();
	}


	public Teacher(String name, String address, String email) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Teacher [name=" + name + ", address=" + address + ", email=" + email + "]";
	}
	
	

}
