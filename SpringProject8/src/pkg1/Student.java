package pkg1;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
  
public class Student {
	List addressList;
	Set addressSet;
	Map addressMap;
	Properties addessProp;
	
	
	public Student() {
		
	}
	public Student(List addressList, Set addressSet, Map addressMap, Properties addessProp) {
		super();
		this.addressList = addressList;
		this.addressSet = addressSet;
		this.addressMap = addressMap;
		this.addessProp = addessProp;
	}
	public List getAddressList() {
		//System.out.println("addressList :"+addressList);
		return addressList;
		
	}
	public void setAddressList(List addressList) {
		this.addressList = addressList;
		 
	}
	public Set getAddressSet() {
		//System.out.println("addressSet :"+addressSet);
		return addressSet;
		
		
	}
	public void setAddressSet(Set addressSet) {
		this.addressSet = addressSet;
	}
	public Map getAddressMap() {
		//System.out.println("addressMap :"+addressMap);
		return addressMap;
	}
	public void setAddressMap(Map addressMap) {
		this.addressMap = addressMap;
	}
	public Properties getAddessProp() {
		//System.out.println("addressProp :"+addessProp);
		return addessProp;
	}
	public void setAddessProp(Properties addessProp) {
		this.addessProp = addessProp;
	}
	@Override
	public String toString() {
		return "Student [addressList=" + addressList + ", addressSet=" + addressSet + ", addressMap=" + addressMap
				+ ", addessProp=" + addessProp + "]";
	}
	
	
}
