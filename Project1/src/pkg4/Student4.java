package pkg4;

import org.springframework.beans.factory.annotation.Autowired;

public class Student4 {
	private String name;
	private String address;
	
	public Student4() {
		
	}

	public Student4(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}
    @Autowired
	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}
     @Autowired
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student4 [name=" + name + ", address=" + address + "]";
	}
   
	
	

}
