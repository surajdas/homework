package pkg3;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test1 {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");
		Student1 student1 = context.getBean("st3", Student1.class);

		System.out.println("Name  :" + student1.getName());
		System.out.println("Email :" + student1.getEmail());

	}

}
