package pkg3;

import org.springframework.beans.factory.annotation.Required;

public class Student1 {
	private String name;
	private String email;

	public Student1() {

	}

	public Student1(String name, String email) {
		 
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}
    @Required
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
    @Required
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student1 [name=" + name + ", email=" + email + "]";
	}
	

}
