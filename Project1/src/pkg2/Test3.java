package pkg2;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test3 {

	public static void main(String[] args) {
	     ClassPathXmlApplicationContext context=new 
	    		 ClassPathXmlApplicationContext("AppContext.xml");
	     MessageInf msg=context.getBean("M1",Message1.class);
	    msg.getMessage();
		 msg=context.getBean("M2",Message2.class);
		 msg.getMessage();
	}

}
