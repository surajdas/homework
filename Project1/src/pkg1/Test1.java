package pkg1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test1 {
	
	public static void main(String[] args)
	{
		ClassPathXmlApplicationContext context=new
				ClassPathXmlApplicationContext("AppContext.xml");
		
		InfStudent st1=context.getBean("st1",Student.class);
		System.out.println(st1.getNum());
		
		st1=context.getBean("st2",Student2.class);
		System.out.println(st1.getNum());
	}

}
