package pkg1;

public class Student {
	 private String name;
	 private String email;
	public Student() {
		super();
	}
	public Student(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", email=" + email + "]";
	}
	 
}
