package json_pojo;

public class Address {
	private String tole;
	private int wardNo;
	private String mpc;
	private String country;
	
	public Address() {}

	public String getTole() {
		return tole;
	}

	public void setTole(String tole) {
		this.tole = tole;
	}

	public int getWardNo() {
		return wardNo;
	}

	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [tole=" + tole + ", wardNo=" + wardNo + ", mpc=" + mpc + ", country=" + country + "]";
	}		
}
