package pkg3;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/person")
public class PersonController {
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor =  new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
	
	@RequestMapping("/showForm")
	public String showForm(Model customerModel) {
		customerModel.addAttribute("person", new Person());		
		return "person_form1";
	}
	
	@RequestMapping("/processForm")
	public String processForm(@Valid @ModelAttribute("person") 
			Person thePerson, BindingResult theBindingResult) {		
		if(theBindingResult.hasErrors()) {
			//Redirect to form
			return "person_form1";
		}
		else {
			return "person-confirmation1";
		}		
	}	
}