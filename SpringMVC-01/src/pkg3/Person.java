package pkg3;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Person {
	
	@Min(value=1, message="must be >=1")
	@Max(value=99, message="must be <=99")	
	private int personId;

	private String fullName;

	public Person() {		
		this.personId = 0;
		this.fullName = "";
	}
	
	public Person(int personId, String fullName) {		
		this.personId = personId;
		this.fullName = fullName;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "Person [personId=" + personId + ", fullName=" + fullName + "]";
	}		
}