package pkg0;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Database {
	SessionFactory factory;
	Session session;
	
	public Database() {
		this.factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();	
		this.session = factory.getCurrentSession();	
	}
	
	public boolean insertRecord(Student student) {
		boolean result = false;
		try {
			session.beginTransaction();
			session.save(student);
			session.getTransaction().commit();					
			result=true;
		}
		catch(Exception ex) {
			
			result=false;
		}
		return result;
	}

	public boolean updateRecord(Student student) {
		boolean result = false;
		try{
			int student_id = student.getId();						
			session.beginTransaction();
			Student student1 = session.get(Student.class, student_id);
			student1.setFirstName(student.getFirstName());
			student1.setLastName(student.getLastName());
			student1.setLastName(student.getEmail());
			session.getTransaction().commit();		
			result = true;
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
			result = false;
		}
		return(result);
	}
	
	public boolean deleteRecord(Student student) {
		boolean result = false;
		try{
			int student_id = student.getId();						
			session.beginTransaction();
			Student student1 = session.get(Student.class, student_id);
			session.delete(student1);
			session.getTransaction().commit();		
			result = true;
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
			result = false;
		}
		return(result);
	}
	
	public Student searchRecord(Student student) {
		Student student1=null;
		try {
			session.beginTransaction();			
			student1= session.get(Student.class, student.getId());
			session.getTransaction().commit();
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
		return student1;
	}
	
	public List<Student> getAll() {
		List<Student> students=new ArrayList<Student>();
		try {
			//Query All Student
			 students = session.createQuery("from Student").list();
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
		return students;
	}
}
