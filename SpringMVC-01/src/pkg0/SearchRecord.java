package pkg0;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class SearchRecord {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();		
		Session session = factory.getCurrentSession();		
		try {			
			System.out.println("Search started....");

			session.beginTransaction();			
			Student student1= session.get(Student.class, 3);
			System.out.println("Student Details:"+student1);			
			session.getTransaction().commit();
			
			System.out.println("End searching ....!");
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}