package package10;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

//range 
public class Filter2 implements Filter {

     
	public void destroy() {
		System.out.println("hello from destroy of filter2");
	 
	}
 
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		int num1=Integer.parseInt(request.getParameter("txt_num1"));
		System.out.println("hello from dofilter of filter2");
		if(num1>=1 && num1<=100) {
			chain.doFilter(request, response);
		}else {
			System.out.println("Number is out of range");
			RequestDispatcher rd=request.getRequestDispatcher("form10.jsp");
			response.setContentType("text/html");
			PrintWriter out=response.getWriter();
			out.println("Number is out of range");
			//rd.forward(request, response);
			rd.include(request, response);
		}
		
		 
		 
	}
 
	public void init(FilterConfig fConfig) throws ServletException {
	System.out.println("Hello from init of filter2");	 
	}

}
