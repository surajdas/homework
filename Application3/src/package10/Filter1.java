package package10;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

public class Filter1 implements Filter {

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("hii from init method of filter1");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		System.out.println("hii from do filter method of filter1");
		String tmp = request.getParameter("txt_num1");
		try {
			System.out.println("inside of try block");
			Integer.parseInt(tmp);
			chain.doFilter(request, response);
		} catch (Exception e) {
			System.out.println("Error " + e.getMessage());
			out.println("Error " + e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher("form10.jsp");

			rd.forward(request, response);
		}

	}

	@Override
	public void destroy() {
		System.out.println("hi from destroy method of filter1");
	}

}
