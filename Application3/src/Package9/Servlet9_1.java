package Package9;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
 
public class Servlet9_1 extends HttpServlet {
	 
    
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     response.setContentType("text/html");
	     
	     int n1=Integer.parseInt(request.getParameter("txt_n1"));
	     int n2=Integer.parseInt(request.getParameter("txt_n2"));
	     int n3=n1+n2;
	     
	     //URL_Rewrite"
	     RequestDispatcher rd=request.getRequestDispatcher("Servlet9?n1="+n1 +"&n2="+n2 +"&n3="+n3);
	     rd.forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doProcess(request, response);	 
	}

	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
	}

}
