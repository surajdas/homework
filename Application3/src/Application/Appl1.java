package Application;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Database;
import database.Message;
import database.User;
 
public class Appl1 extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 response.setContentType("text/html");
	 PrintWriter out=response.getWriter();
	 
	 String  login_name=request.getParameter("login_name");
	 String login_password=request.getParameter("login_password");
	   RequestDispatcher rd;
	   User user=new User();
	   user.setLogin_Name(login_name);
	   user.setLogin_Password(login_password);
	    
	   Database db=new Database();
	   Message mesag=db.doLogin(user);
	   user=mesag.getUser();
	 if(mesag.isFlag()) {
		 request.setAttribute("full_Name",mesag.getUser().getFull_Name());
		 rd=request.getRequestDispatcher("Next");
		 rd.forward(request, response);
	 }
	 else {
		 rd=request.getRequestDispatcher("index.jsp");
		 rd.include(request, response);
	 }
	 System.out.println(user);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 	
		  doProcess(request, response);
	}
 
	 
}
