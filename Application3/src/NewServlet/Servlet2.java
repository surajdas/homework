package NewServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
public class Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}
 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 response.setContentType("text/html");
	 PrintWriter out=response.getWriter();
	 ServletContext sc=getServletContext();
     String site_name=sc.getInitParameter("site_name");
     out.println(site_name);
	 
     ServletConfig scg=getServletConfig();
     String servlet_name=scg.getInitParameter("servlet_titl2");
     out.println("<br>Servlet name:"+servlet_name);
	 
	 out.println("<br>Hello from servlet2");
	 out.println("<a href='Servlet1'>servlet1</a>");
	 
	}

}
