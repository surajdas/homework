package cookies;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Servlet7 extends HttpServlet {
 
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter(); 
		
		Cookie[] cks=request.getCookies();
		 String str=request.getParameter("ck1");
		 String str1=request.getParameter("ck2");
		 
		 if(cks.length>0) {
			 for(int i=0;i<cks.length;i++) {
				 
				out.println(cks[i].getName()+ "="+cks[i].getValue()+"<br>");
			 }
		 }
		 System.out.println("total cookies:"+cks.length);
		 
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}

}
