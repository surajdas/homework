package Package7;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
 
public class Servlet3 extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out=response.getWriter();
		
		
		ServletContext sc=getServletContext();
	        String site_name=sc.getInitParameter("site_name");
	        out.println(site_name+"<br>");         
	        out.println("Site_Name:"+site_name);
	          
	        ServletConfig scg=getServletConfig();
	        String servlet_name=scg.getInitParameter("Servlet_title");
	        
	       HttpSession session=request.getSession();
	       
	     //  session.setAttribute("V1", 20);
	     //  session.setAttribute("v2",30);
	     //  int n1=45;
	       int n1=Integer.parseInt(request.getParameter("txt_n1"));
	       session.setAttribute("V1", n1);
	        
	        out.println("<br>Servlet name:"+servlet_name);
	        
	        out.println("hello from srvlet3"+"<br>");
	        out.println("v1="+session.getAttribute("V1"));
	     // out.println("v3="+session.getAttribute("V3"));
	        
	        out.println("<a href='Servlet4'>Servlet4</a>" );
	        out.println("<a href='Servlet5'>Servlet5</a>" );
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
	}

}
