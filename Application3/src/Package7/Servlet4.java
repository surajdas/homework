package Package7;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Servlet4 extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		 ServletContext sc=getServletContext();
	        String site_name=sc.getInitParameter("site_name");
	        out.println(site_name+"<br>");         
	        out.println("Site_Name:"+site_name);
	          
	        ServletConfig scg=getServletConfig();
	        String servlet_name=scg.getInitParameter("Servlet_title");
	        out.println("<br>Servlet name:"+servlet_name);
	        
	        out.println("hello from srvlet4"+"<br>");
	        
	        out.println("v1="+request.getSession().getAttribute("V1")+"<br>");
	        out.println("<a href='Servlet3'>Servlet3</a>" );
	        out.println("<a href='Servlet5'>Servlet5</a>" );
	}
	 
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			 doProcess(request, response);
		}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    doProcess(request, response);
	}

}
