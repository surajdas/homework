package Package7;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Servlet5 extends HttpServlet {
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
	}

	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		 out.println("v1="+request.getSession().getAttribute("V1")+"<br>");
		 out.println("<a href='Servlet3'>Servlet3</a>" );
		 out.println("<a href='Servlet4'>Servlet4</a>" );
	}

}
