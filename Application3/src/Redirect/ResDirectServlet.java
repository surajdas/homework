package Redirect;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
 
public class ResDirectServlet extends HttpServlet {
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}
 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="http://google.com.np";
		//if url exit or not(google search)
		
		response.setContentType("text/html");
		PrintWriter out= response.getWriter();
		response.sendRedirect(url);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
		}
	 
	
	

}
