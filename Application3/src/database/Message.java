package database;

public class Message {
	User user;
	boolean flag;
	String message;
	 
	public Message()
	{ 
		this.user=new User();
		this.message="";
		this.flag=false;
	}
	public Message(User user,boolean flag,String message) {
		this.user=user;
		this.flag=flag;
		this.message =message;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Message [user=" + user + ", flag=" + flag + ", message=" + message + "]";
	}
	
	
}
