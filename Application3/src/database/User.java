package database;

public class User {
	
	private int id;
	private String full_Name;
	private String login_Name;
	private String login_Password;
	public User() {
		 
	}
	public User(int id, String full_Name, String login_Name, String login_Password) {
		super();
		this.id = id;
		this.full_Name = full_Name;
		this.login_Name = login_Name;
		this.login_Password = login_Password;
	}
	public User(User user) {
		super();
		this.id = user.id;
		this.full_Name = user.full_Name;
		this.login_Name = user.login_Name;
		this.login_Password = user.login_Password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFull_Name() {
		return full_Name;
	}
	public void setFull_Name(String full_Name) {
		this.full_Name = full_Name;
	}
	public String getLogin_Name() {
		return login_Name;
	}
	public void setLogin_Name(String login_Name) {
		this.login_Name = login_Name;
	}
	public String getLogin_Password() {
		return login_Password;
	}
	public void setLogin_Password(String login_Password) {
		this.login_Password = login_Password;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", full_Name=" + full_Name + ", login_Name=" + login_Name + ", login_Password="
				+ login_Password + "]";
	}
	
	

}
