package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class Database {
	
	private PreparedStatement pstat;
	private Connection conn;
	private ResultSet rs;
	String str_sql;
	boolean result=false;
	public Message doLogin(User user)
	{
		 
		try {
			 
			Class.forName("com.mysql.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
			str_sql="select *from tbl_user where login_Name=? and login_password=?";
			pstat=conn.prepareStatement(str_sql);
			pstat.setString(1, user.getLogin_Name());
			pstat.setString(2, user.getLogin_Password());
			 rs=pstat.executeQuery();
			 while(rs.next()) {
				 user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
				 result=true;
			 }
			 
			pstat.close();
			conn.close();
			 
			  if(result==true) {
				  return new Message(user,true, "Sucess");
			  }else {
				  return new Message(user,false,"Error");
			  }
		} catch (Exception e) {
		 System.out.println(e.getMessage());	
		 return new Message(user,false,e.getMessage());
		}
		 
	}
	
	

}
