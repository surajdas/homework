package package11;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.PreparedStatement;
 
public class Db_Connect extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		Connection conn;
		PreparedStatement pstat;
		String url="jdbc:mysql://";
		String host="localhost";
		String port="3306";
		String dbname="test1";
		String username="root";
		String userpass="";
		 
		 try {
		 Class.forName("com.mysql.jdbc.Driver");
		 conn=DriverManager.getConnection(url+host+":"+port +"/"+dbname,username,userpass);
		 response.getWriter().println("<p>connect database sucessfully</p>");
		 
		 conn.close();
		 
		 }catch (Exception e) {
			 response.getWriter().println("<p>:Error"+e.getMessage());
		}
	}

	 
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			 doProcess(request, response);
		}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

}
