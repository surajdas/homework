package package11;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.PreparedStatement;
 
public class InsertRecord extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		Connection conn;
		PreparedStatement pstat;
		 
		String url="jdbc:mysql://";
		String host="localhost";
		String port="3306";
		String dbname="test1";
		String username="root";
		String userpass="";
		 	
		 try {
			 String full_name=request.getParameter("full_name");
			 String contact_address=request.getParameter("contact_address");
			 String str_sql="insert into tbl_person(full_name,contact_address)values(?,?)";			 
			 Class.forName("com.mysql.jdbc.Driver");
			 
		 conn=DriverManager.getConnection(url+host+":"+port +"/"+dbname,username,userpass);
		 
		 pstat=conn.prepareStatement(str_sql);
		 pstat.setString(1, full_name);
		 pstat.setString(2, contact_address);
		 pstat.executeUpdate();
		 response.getWriter().println("<p>insert record sucessfully</p>");
		 pstat.close();
		 conn.close();
		 
		 }catch (Exception e) {
			 response.getWriter().println("<p>:Error"+e.getMessage());
		}
	}

	 
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			 doProcess(request, response);
		}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

}
