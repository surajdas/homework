package package11;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
 
public class SearchRecord1 extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		Connection conn;
		PreparedStatement pstat;
		 
		String url="jdbc:mysql://";
		String host="localhost";
		String port="3306";
		String dbname="test1";
		String username="root";
		String userpass="";
		 	
		 try {
			 int id=Integer.parseInt(request.getParameter("txt_id"));
			  
			 String str_sql="select * from tbl_person where id=?";			 
			 Class.forName("com.mysql.jdbc.Driver");
			 
		 conn=DriverManager.getConnection(url+host+":"+port +"/"+dbname,username,userpass);
		 
		 pstat=conn.prepareStatement(str_sql);
		 pstat.setInt(1, id);
		ResultSet rs=pstat.executeQuery();
		boolean res=false;
		while (rs.next()) {  
			res=true;
			response.getWriter().println("Id:"+rs.getInt(1) +"<br>");
			response.getWriter().println("Name:"+rs.getString(2) +"<br>");
		    response.getWriter().println("Address:"+rs.getString(3) +"<br>");
		    request.setAttribute("id",rs.getInt(1));
			request.setAttribute("Name", rs.getString(2));
			request.setAttribute("address",rs.getString(3));
		}
		
		 
		
		RequestDispatcher rd=request.getRequestDispatcher("Form11_2.jsp");
		rd.include(request, response); 
		
		// pstat.executeUpdate();
		 pstat.close();
		 conn.close();
		 if(res=true) {
			 response.getWriter().println( "<p>record found suceesfully </p>");
		 }else {
			 response.getWriter().println("<p>record not found suceesfully </p>");
			   rd=request.getRequestDispatcher("SearchForm1.jsp");
				rd.include(request, response); 
		 }
		 }catch (Exception e) {
			 
			 response.getWriter().println("<p>:Error"+e.getMessage());
		}
	}

	 
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			 doProcess(request, response);
		}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

}
