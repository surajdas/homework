package pk1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Database {
	
	private PreparedStatement pstat;
	private ResultSet rs;
	private Connection conn;
	String str_sql;
	public Message doLogIn(User user) {
		boolean result=false;
		try {
			 
			Class.forName("com.mysql.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
			str_sql="select *from tbl_user where login_Name=? and login_Password=?";
			pstat=conn.prepareStatement(str_sql);
			 
			pstat.setString(1,user.getLogin_Name());
			pstat.setString(2, user.getLogin_Pasword());
			rs=pstat.executeQuery();
			
			while(rs.next()) {
				user=new User(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4));
				result=true;
			}
			System.out.println(user);
			if(result==true) {
			   return new Message("Sucess", true, user);
			}else {
				 return new Message("Error",false, user);
			}
		} catch (Exception e) {
			 return new Message(e.getMessage(),false, user);
		}
	}

}
