package pk1;

public class User {
	
	private int id;
	private String full_Name;
	private String login_Name;
	private String login_Pasword;
	public User() {
		 
	}
	public User(int id, String full_Name, String login_Name, String login_Pasword) {
		super();
		this.id = id;
		this.full_Name = full_Name;
		this.login_Name = login_Name;
		this.login_Pasword = login_Pasword;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFull_Name() {
		return full_Name;
	}
	public void setFull_Name(String full_Name) {
		this.full_Name = full_Name;
	}
	public String getLogin_Name() {
		return login_Name;
	}
	public void setLogin_Name(String login_Name) {
		this.login_Name = login_Name;
	}
	public String getLogin_Pasword() {
		return login_Pasword;
	}
	public void setLogin_Pasword(String login_Pasword) {
		this.login_Pasword = login_Pasword;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", full_Name=" + full_Name + ", login_Name=" + login_Name + ", login_Pasword="
				+ login_Pasword + "]";
	}
	
	
	

}
