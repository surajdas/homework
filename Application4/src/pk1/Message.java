package pk1;

public class Message {
	
	  String message;
      boolean flag;
      User user;
    
    
	public Message() {
		this.message="";
		 this.flag=false;
		 this.user=new User();
	}


	public Message(String message, boolean flag, User user) {
		super();
		this.message = message;
		this.flag = flag;
		this.user = user;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Message [message=" + message + ", flag=" + flag + ", user=" + user + "]";
	}
	
    
    
}
