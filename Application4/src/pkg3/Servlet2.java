package pkg3;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Servlet2 extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/html");
	   PrintWriter out=response.getWriter();
	   ServletContext sco=getServletContext();
       
	   
	   ServletConfig sconfg=getServletConfig();
	   String str2=sconfg.getInitParameter("param3");
	    
	   out.println("<h1>Hello From Servlet2 </h1><br>");
	   String str1=sco.getInitParameter("param1");
	   out.println(str1 +"<br>");
	   out.println(str2+"<br>");
	   out.println("<a href='pass'>Servlet1</a>");
	   
	   
	   
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
	}

}
