package pkg3;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Servlet1 extends HttpServlet {
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doProcess(request, response);
	}

 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  response.setContentType("text/html");
		   PrintWriter out=response.getWriter();
		   ServletContext sc=getServletContext();
		   
		   ServletConfig sconfg=getServletConfig();
		   String str2=sconfg.getInitParameter("param2");
		   
		   
		   
		   String site_name=sc.getInitParameter("param1");
		   out.println("Hello From Servlet1 <br>");
		   out.println(site_name +"<br>");
		   out.println(str2 +"<br>");
		   out.println("<a href='out'>Servlet2</a> <br>");
	}

}
