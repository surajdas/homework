package pk2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pk1.Database;
import pk1.Message;
import pk1.User;
 
public class Login extends HttpServlet {
 
    
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String login_Name=request.getParameter("login_name");
		String login_password=request.getParameter("login_password");		
		RequestDispatcher rd;
		User user=new User();
		user.setLogin_Name(login_Name);
		user.setLogin_Pasword(login_password);
		
		Database db=new Database();
		Message mess=db.doLogIn(user);
		user=mess.getUser();
		 if(mess.isFlag()) {
			 
			 request.setAttribute("full_name",mess.getUser().getFull_Name());
			 rd=request.getRequestDispatcher("web2");
			 rd.forward(request, response);
			 
		 }else {
			 rd=request.getRequestDispatcher("index.jsp");
			 rd.include(request, response);
		 }
		 
		
		
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
	}
}
