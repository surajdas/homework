package p1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class First extends GenericServlet {

	@Override
	public void service(ServletRequest request, ServletResponse response ) throws ServletException, IOException {
		response.setContentType("html\txt");
		PrintWriter out=response.getWriter();
		out.println("<h3>hello world of servlet</h3>");
		out.println("Hii I am Suraj");
		out.close();

	}

}
