package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sound.midi.Soundbank;

import Login.Message;

public class DataBase1 {
	 
  public Message doLogin(User user) {
	  //load driver
	  PreparedStatement pstat;
	  ResultSet rs=null;
	  Connection conn;
	  String str_sql;
	  boolean result=false;
	  
	 try {
		// System.out.println(user);
		Class.forName("com.mysql.jdbc.Driver");
		conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
		//System.out.println("25 true");
	    str_sql="select * from tbl_user where login_Name=? and login_Password=?";
	   
	    pstat=conn.prepareStatement(str_sql);
	    pstat.setString(1, user.getFull_Name());
	    pstat.setString(2, user .getLogIn_Name());
	    pstat.setString(3, user.getLogIn_Password());
	   pstat.executeQuery();
	   while(rs.next())
	   {
		user=new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4));
		 
	   }
	   result=true;
	   System.out.println(user);
	     pstat.close();
	     conn.close();
	     result=true;
	   if(result) {
		   return new Message(user,true,"Sucess");
	   }
	   else {
		   return new Message(user,false,"Error");
	   }
	} catch (Exception e) {
		 return new Message(user,false,e.getMessage()); 
	} 
	 
  }
  
  
  public Message saveUser(User user) {
	  //load driver
	  PreparedStatement pstat;
	  ResultSet rs;
	  Connection conn;
	  String str_sql;
	  boolean result=false;
	  
	 try {
		// System.out.println(user);
		Class.forName("com.mysql.jdbc.Driver");
		conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
		//System.out.println("25 true");
	    str_sql="insert into tbl_user(full_Name,login_Name,login_password)values(?,?,?)";
	    System.out.println(str_sql);
	    pstat=conn.prepareStatement(str_sql);
	    pstat.setString(1, user.getLogIn_Name());
	    pstat.setString(2, user.getLogIn_Password());
	    rs=pstat.executeQuery();
	    while ( rs.next()) {
			 
			  user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));		 
			  
		}
	    result =true;	 
	    System.out.println(user);
	    rs.close();
	    pstat.close();
	    conn.close();
	   if(result) {
		   return new Message(user,true,"Sucess");
	   }
	   else {
		   return new Message(user,false,"Error");
	   }
	} catch (Exception e) {
		 return new Message(user,false,e.getMessage()); 
	} 
	 
  }
}
