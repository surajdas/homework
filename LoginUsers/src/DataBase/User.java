package DataBase;

public class User {
	
	private int id;
	private String full_Name;
	private String login_Name;
	private String login_Password;
	
	public User()
	{
		
	}
	public User(int id,String name,String lgin_Name,String lgin_Password)
	{
		this.id=id;
		this.full_Name=name;
		this.login_Name=lgin_Name;
		this.login_Password=lgin_Password;
	}
	public User(User data)
	{
		this.id=id;
		this.full_Name= full_Name;
		this.login_Name=login_Name;
		this.login_Password=login_Password;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	public int getId() {
		return id;
	}
	public void setFull_Name(String full_Name) {
		this.full_Name=full_Name;
	}
	public String getFull_Name()
	{
		return full_Name;
	}
	public void setLogin_Name(String login_Name)
	{
		this.login_Name=login_Name;
	}
	public String getLogIn_Name() {
		return login_Name;
	}
	public void setLogIn_Password(String login_Password) {
		this.login_Password=login_Password;
	}
	public String getLogIn_Password() {
		return login_Password;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", full_Name=" + full_Name + ", login_Name=" + login_Name + ", login_Password="
				+ login_Password + "]";
	}
	
	
	

}
