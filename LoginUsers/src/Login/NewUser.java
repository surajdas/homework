package Login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DataBase.DataBase1;
import DataBase.User;
 
public class NewUser extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		 String full_Name=request.getParameter("txt_Name");
		 String login_Name=request.getParameter("txt_login_Name");
		 String login_password=request.getParameter("txt_password");
		 User user=new User(0, full_Name, login_Name, login_password);
		 DataBase1 db=new DataBase1();
		
		  Message message=db.saveUser(user);
		 if(message.flag) {
		  out.println("save new user sucessfully");
		 }else {
			 out.println("Error:to save user");
		 }
		 out.println("<a href='Accept'>Back</a>");
	}
	
	 
}
