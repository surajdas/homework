package Login;

import java.io.IOException;
import java.io.PrintWriter;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
 
public class SaveUser extends HttpServlet {
	 
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	 response.setContentType("text/html");
	 PrintWriter out=response.getWriter();
	 String fn=request.getParameter("txt_fn");
	 String ln=request.getParameter("txt_ln");
	 String lp=request.getParameter("txt_lp");
	 try {
		  Class.forName("com.mysql.jdbc.Driver");
		 Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
		 String sql="insert into tbl_user(full_Name,login_Name,login_password) values(?,?,?)";
		 PreparedStatement pstat=conn.prepareStatement(sql);
		 pstat.setString(1,fn);
		 pstat.setString(2, ln);
		 pstat.setString(3, lp);
		 int res=pstat.executeUpdate();
		 pstat.close();
		 conn.close();
		 out.println("insert");
		  
	} catch (Exception e) {
	   out.println("Error"+e.getMessage());
	}
	 
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doProcess(request, response);
	}
  

}
