package Server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import databasework.Database;
import databasework.Message;
import databasework.User;
 
public class Servlet1 extends HttpServlet {
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		 
         User user=new User();
         user.setLogin_Name(name);
         user.setLogin_password(password);
         Database db=new Database();
         Message mes=db.doLogIn(user);
         user=mes.getUser();
         System.out.println(mes.isFlag());
         /*
		if(name.equals("admin")&&password.equals("admin"))
		{
			RequestDispatcher rd=request.getRequestDispatcher("web1");
			rd.forward(request, response);
		}
		else {
			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
			rd.include(request, response);
		}*/
	
        if(mes.isFlag()==true){
        	 
        	request.setAttribute("full_Name",mes.getUser().getFull_Name());
 			RequestDispatcher rd=request.getRequestDispatcher("web1");
 		    
 			rd.forward(request, response);
 		}
 		else {
 			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
 			rd.include(request, response);
 		
		
		
	}
 
	}

}
