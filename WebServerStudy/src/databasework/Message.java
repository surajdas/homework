package databasework;

public class Message {
	 
	String message;
    boolean flag=false;
    User user;
    public Message()
    {
    	this.message="";
    	this.flag=false;
    	this.user=new User();
    }
    public Message(String mes,boolean result,User user)
    {
    	this.message=mes;
    	this.flag=result;
    	this.user=user;
    }
    public Message(Message m) {
    	this.user=m.user;
    	this.flag=m.flag;
    	this.message=m.message;
    }
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
    


}
