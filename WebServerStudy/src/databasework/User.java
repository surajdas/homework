package databasework;

public class User {
	
	private int id;
	private String full_Name;
	private String login_Name;
	private String login_password;
	
	
	public User()
	{
		
	}
	public User(int id,String full_Name,String login_Name,String login_Password) {
		this.id=id;
		this.full_Name=full_Name;
		this.login_Name=login_Name;
		this.login_password=login_password;
	}
	public User(User user)
	{
		this.id=id;
		this.full_Name=full_Name;
		this.login_Name=login_Name;
		this.login_password=login_password;
		
	}
	public void setId(int id)
	{
		this.id=id;
	}
	public int getId()
	{
		return id;
	}
    public void setFull_Name(String full_Name) {
    	this.full_Name=full_Name;
    }
    public String getFull_Name()
   {
    	return full_Name;
     }
    public void setLogin_Name(String login_Name)
    {
    	this.login_Name=login_Name;
    }
    public String getLogIn_Name()
    {
    	return login_Name;
    }
    public void setLogin_password(String login_password)
    {
    	this.login_password=login_password;
    }
    public String getLogin_Password()
    {
    	return login_password;
    }
	@Override
	public String toString() {
		return "User [id=" + id + ", full_Name=" + full_Name + ", login_Name=" + login_Name + ", login_password="
				+ login_password + "]";
	}
    
}
