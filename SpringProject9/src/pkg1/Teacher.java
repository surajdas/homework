package pkg1;

public class Teacher {
       private Student student;
       private String number;
       public Teacher() {
   		super();
   		this.student = new Student();
   		this.number = "456065478";
   	} 
       
	public Teacher(Student student, String number) {
		super();
		this.student = student;
		this.number = number;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	@Override
	public String toString() {
		return "Teacher [student=" + student + ", number=" + number + "]";
	}
       
	public void st() {
		student.teacher();
	}
       
}
