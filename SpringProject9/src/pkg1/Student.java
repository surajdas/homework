package pkg1;

public class Student {
	 
	private String name;
	private String email;
	private String address;
	
	
	public Student() {
		this.name="suraj das";
		this.email="sd@gmail.com";
		this.address="kathmandu";
	}


	public Student(String name, String email, String address) {
		super();
		this.name = name;
		this.email = email;
		this.address = address;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	@Override
	public String toString() {
		return "Student [name=" + name + ", email=" + email + ", address=" + address + "]";
	}
	
	
	public void teacher() {
		System.out.println("Suraj Babu");
	}


}
