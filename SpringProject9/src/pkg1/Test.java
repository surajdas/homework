package pkg1;


import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
      public static void main(String[] args) {
    	  ClassPathXmlApplicationContext context=new 
    			  ClassPathXmlApplicationContext("AppContext.xml");
    	  
    	  Student st1=context.getBean("s1",Student.class);
    	  System.out.println(st1);
    	  
    	  Teacher t1=context.getBean("s2",Teacher.class);
    	  System.out.println(t1);
    	  
    	  t1.st();
		
	}
}
