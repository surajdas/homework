package pkg1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student implements StudentInf {
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "full_name")
	private String fullName;
	@Column(name = "email")
	private String email;
	@Column(name = "address")
	private String address;

	public Student() {
		this.id = 1;
		this.fullName = "Suraj Baniya";
		this.email = "sd@gmail.com";
		this.address = "ktm";
	}

	public Student(int id, String fullName, String email, String address) {

		this.id = id;
		this.fullName = fullName;
		this.email = email;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", fullName=" + fullName + ", email=" + email + ", address=" + address + "]";
	}

	@Override
	public void getMessage() {
		System.out.println("hello");

	}

}
