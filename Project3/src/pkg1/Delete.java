package pkg1;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Delete {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			 
			 System.out.println("enter id");
			 int id=new Scanner(System.in).nextInt();
			 Student s1=session.get(Student.class, id);
			 session.delete(s1);
			 
			session.beginTransaction().commit();

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
	}
}
