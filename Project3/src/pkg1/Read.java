package pkg1;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Read {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			StudentInf st1 = context.getBean("st3", Student.class);
			 int id=1;
			// Student st=session.get(Student.class, id);
			// System.out.println(st);
			 
			 List<Student>stu=session.createQuery("from Student").list();
			 for(Student s1:stu) {
				 System.out.println("Details"+s1);
			 }
			//session.beginTransaction().commit();
			 session.getTransaction().commit();
			 

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
	}
}
