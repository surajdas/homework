package pkg1;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainTest {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			StudentInf st1 = context.getBean("st3", Student.class);
			session.save(st1);
			StudentInf st2 = context.getBean("st4", Student.class);
			session.save(st2);
			session.beginTransaction().commit();
             session.close();
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
	}

}
