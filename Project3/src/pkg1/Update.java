package pkg1;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Update {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			System.out.println("Enter id");
			int id = new Scanner(System.in).nextInt();

			Student st1 = session.get(Student.class, id);

			System.out.print("Enter worker Name:");
			String name = new Scanner(System.in).nextLine();

			System.out.print("Enter the worker email:");
			String email = new Scanner(System.in).nextLine();

			System.out.print("Enter the worker address:");
			String address = new Scanner(System.in).nextLine();

			st1.setFullName(name);
			st1.setEmail(email);
			st1.setAddress(address);

			session.beginTransaction().commit();

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
	}
}
