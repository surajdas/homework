package pkg1;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class searchRecord {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			StudentInf st1 = context.getBean("st3", Student.class);
			 int id=1;
			 Student st=session.get(Student.class, id);
			 System.out.println(st);
			 
			 
			session.beginTransaction().commit();

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
	}
}
