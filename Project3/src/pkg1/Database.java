package pkg1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Database {

	SessionFactory factory;
	Session session;
	boolean result = false;
	ClassPathXmlApplicationContext context;

	public Database() {

		this.factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		this.session = factory.getCurrentSession();
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");
	}

	public boolean insertRecord(Student student) {
		try {

			session.beginTransaction();		 
			session.save(student);
			session.beginTransaction().commit();
			result = true;
			return result;
		} catch (Exception e) {
			System.out.println("Error " + e.getMessage());
			result = false;
			return result;
		}
	}

	public boolean updateRecord(Student student) {
		try {

			session.beginTransaction();
			int id = student.getId();
			Student st1 = session.get(Student.class, id);

			st1.setFullName(student.getFullName());
			st1.setEmail(student.getEmail());
			st1.setAddress(student.getAddress());

			session.beginTransaction().commit();

			result = true;
			return result;
		} catch (Exception e) {
			System.out.println("Error :");
			result = false;
			return result;
		}
	}

	public boolean deleteRecord(Student student) {
		try {

			session.beginTransaction();

			int id = student.getId();
			Student s1 = session.get(Student.class, id);
			session.delete(s1);

			session.beginTransaction().commit();

			result = true;
			return result;
		} catch (Exception e) {
			System.out.println("Error :");
			result = false;
			return result;
		}
	}

	public Student searchRecord(Student student) {
            Student st=null;
		try {
			session.beginTransaction();
			 
			  st = session.get(Student.class, student.getId());

			session.beginTransaction().commit();

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());

		}
		return st;
	}
	
	public List<Student>getAll() {
		 
		 List<Student> student=new ArrayList<Student>();
		try {
			session.beginTransaction();
			student=session.createQuery("from Student").list();
			session.beginTransaction().commit();
		} catch (Exception e) {
		 System.out.println("error:"+e.getMessage());
		}
		 return student;
	}

}
