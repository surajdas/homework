package pkg1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {
	
	public static void main(String[] args) {
		SessionFactory factory=new 
	Configuration().configure("hibernate.cfg.xml").
	addAnnotatedClass(Student.class).buildSessionFactory();
		
		
		Session session=factory.getCurrentSession();
		
		
		try {
			Student st1=new Student(2, "Suraj", "Das", "sd@gmail.com");
			//start translation
			session.beginTransaction();
			//save student object
			session.save(st1);
			//commit translation
			session.getTransaction().commit();
			
		} catch (Exception e) {
			 System.out.println("Error:"+e.getMessage());
		}
		
	}

}
