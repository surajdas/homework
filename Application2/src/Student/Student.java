package Student;
public class Student {
 
	
	  private int id;
	  private String full_Name;
	  private String login_password;
	  private String login_Name;
	 
	  
	public Student() {
	 
	}

	public Student(int userId, String full_Name, String login_password, String login_Name) {
		 
		this.id = userId;
		this.full_Name = full_Name;
		this.login_password = login_password;
		this.login_Name = login_Name;
	}
	
	public Student(Student std) {
		this.id = id;
		this.full_Name = full_Name;
		this.login_password = login_password;
		this.login_Name = login_Name;
	}
	 
	public int getUserId() {
		return id;
	}
	public void setUserId(int userId) {
		this.id = userId;
	}
	public String getFull_Name() {
		return full_Name;
	}
	public void setFull_Name(String full_Name) {
		this.full_Name = full_Name;
	}
	public String getLogin_password() {
		return login_password;
	}
	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}
	public String getLogin_Name() {
		return login_Name;
	}
	public void setLogin_Name(String login_Name) {
		this.login_Name = login_Name;
	}
	@Override
	public String toString() {
		return "Student [userId=" + id + ", full_Name=" + full_Name + ", login_password=" + login_password
				+ ", login_Name=" + login_Name + "]";
	}
	  
	  
	  
}
