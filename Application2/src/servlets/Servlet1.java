package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Student.DataBase;
import Student.Student;
 
public class Servlet1 extends HttpServlet {
 
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out=response.getWriter();
		 
		 
		 String lo_Name=request.getParameter("login_Name");
		 String lo_password=request.getParameter("login_password");
		 Student student=new Student();
		 student.setLogin_Name(lo_Name);
		 student.setLogin_password(lo_password); 
		 DataBase db=new DataBase();
		 Message message=db.doLogin(student);  
			student=message.std;	 
  /*  if(lo_Name.equals("admin")&&lo_password.equals("admin")){
		 RequestDispatcher rd=request.getRequestDispatcher("ser2");
		 rd.forward(request, response);
	}else {
		 RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
		 rd.forward(request, response);
	}*/
		 if(message.flag==true){
			 request.setAttribute("full_Name",message.std.getFull_Name());
			 RequestDispatcher rd=request.getRequestDispatcher("ser2");
			 rd.forward(request, response);
		}else {
			 RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
			 rd.forward(request, response);
		}
		 out.close();
		 
		 
	}
 
}
