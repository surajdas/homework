package HomeWork;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Soundbank;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;
 
 
public class Index5 extends HttpServlet {
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		int n1=Integer.parseInt(request.getParameter("Num1"));
		int n2=Integer.parseInt(request.getParameter("Num2"));
	    int n3=n1+n2;
	    System.out.println("Result::"+n3);
	    request.setAttribute("result",n3);
	   RequestDispatcher rd=request.getRequestDispatcher("Choose");
	    rd.forward(request, response);
	    out.close();
	} 
 

}
