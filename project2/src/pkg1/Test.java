package pkg1;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

import pkg2.InformInf;

public class Test {

	public static void main(String[] args) {
		XmlBeanFactory factory = new XmlBeanFactory(new ClassPathResource("AppContext.xml"));
		InformInf in1 = (InformInf) factory.getBean("m1");
		in1.getInfo();
         
	}

}
