package pkg2;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args)
    {
    	ClassPathXmlApplicationContext context=new 
    ClassPathXmlApplicationContext("AppContext.xml");
    	
    	InformInf inf=context.getBean("m1",MessageImpl.class);
    	System.out.println(inf);
    	
    	inf =context.getBean("m2",MessageImpl.class);
    	System.out.println(inf);
    	
    	inf=context.getBean("m3",MessageImpl.class);
    	System.out.println(inf);
    	
    	inf=context.getBean("m4",MessageImpl.class);
    	System.out.println(inf);
    	
       
    }
}
