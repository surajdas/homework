package pkg2;

public class MessageImpl implements InformInf{
          private String name;
          private int id;
          private String address;
		public MessageImpl() {
			 this.name="Suraj Das";
			 this.id=46;
			 this.address="Sarlahi";
		}
		public MessageImpl(String name, int id, String address) {
			 
			this.name = name;
			this.id = id;
			this.address = address;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		@Override
		public String toString() {
			return "MessageImpl [name=" + name + ", id=" + id + ", address=" + address + "]";
		}
		@Override
		public void getInfo() {
			 System.out.println("Hello ,cute");
			
		}
          
          
}
