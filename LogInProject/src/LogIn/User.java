package LogIn;

public class User {
  private  int id;
  private String full_Name;
  private String login_Name;
  private String logIn_Password;
   
  
    public User() {
	  
      }
    
 
public User(int id, String full_Name, String login_Name, String logIn_Password) {
		 
		this.id = id;
		this.full_Name = full_Name;
		this.login_Name = login_Name;
		this.logIn_Password = logIn_Password;
	}
public User(User user) {
	 
	this.id = id;
	this.full_Name = full_Name;
	this.login_Name = login_Name;
	this.logIn_Password = logIn_Password;
}


public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getFull_Name() {
	return full_Name;
}

public void setFull_Name(String full_Name) {
	this.full_Name = full_Name;
}

public String getLogin_Name() {
	return login_Name;
}

public void setLogin_Name(String login_Name) {
	this.login_Name = login_Name;
}

public String getLogIn_Password() {
	return logIn_Password;
}

public void setLogIn_Password(String logIn_Password) {
	this.logIn_Password = logIn_Password;
}
  
@Override
public String toString() {
	return "User [id=" + id + ", full_Name=" + full_Name + ", login_Name=" + login_Name + ", logIn_Password="
			+ logIn_Password + "]";
  
   
 }
   
}
