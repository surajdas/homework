package Second;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class web extends GenericServlet {

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
        res.setContentType("text/html");
        PrintWriter out=res.getWriter();
        out.println("<html>");
        out.println("<head><title>First page</title></head>");
        out.println("<body>");
        out.println("<h1>First Servlet </h1>");
        out.println("<p><a href='index.jsp'>Home</a></p>");
        out.println("</body>");
        out.println("</html>");
        
        out.close();
	}

}
