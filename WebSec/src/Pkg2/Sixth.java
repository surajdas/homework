package Pkg2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class Sixth extends HttpServlet {
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		String s1=request.getParameter("Num1");
		String s2=request.getParameter("Num2");
		int result=(int)request.getAttribute("result");
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("Num1="+s1+"<br>");
		out.println("Num2="+s2+"<br>");
		out.println("Result="+result+"<br>");
		out.close();
		 
	}

	 

}
