package pkg1;

public class Student implements StudentImpl{
      private String message;
      
      public Student() {
    	  
      }
      public Student(String message) {
    	  this.message=message;
      }
      public void setMessage(String message) {
    	  this.message=message;
      }
      public void getMessage() {
    	  System.out.println("your message :" +message);
      }
      public void init() {
    	  System.out.println("Bean is going through init");
      }
      public void destroy() {
    	  System.out.println("Bean is going to destroy");
      }
	@Override
	public String toString() {
		return "Student [message=" + message + "]";
	}
	@Override
	public void message() {
		 System.out.println("Suraj Das");
		
	}
      
      
}
