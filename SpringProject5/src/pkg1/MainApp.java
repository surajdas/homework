package pkg1;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
	public static void main(String[] args) {

		AbstractApplicationContext context = new ClassPathXmlApplicationContext("AppContext.xml");

		Student st1 = (Student) context.getBean("s1");
		st1.getMessage();
		InitClass st2 = (InitClass) context.getBean("s2");

		System.out.println(st2);
		context.registerShutdownHook();

	}
}
