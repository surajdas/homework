package pkg1;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class InitClass implements BeanPostProcessor  {
      
	public Object PostProcessBeforeInitailization(Object bean,String beanName)
			throws BeansException{
		System.out.println("BeforeInitialization :"+beanName);
		return bean;
	}
	
	
	   
		public Object PostProcessAfterInitailization(Object bean,String beanName)
				throws BeansException{
			System.out.println("BeforeInitialization :"+beanName);
			return bean;
		}
		
}
