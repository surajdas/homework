package package1;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class InitClass implements BeanPostProcessor{
	
	
	public Object postprocessBeforeInitialization(Object bean,String beanName) throws BeansException{
		System.out.println("beforeInitailization "+beanName);
		return bean;
	}
	
	public Object postprocessAfterInitialization(Object bean,String beanName) throws BeansException{
		System.out.println("AfterInitailization "+beanName);
		return bean;
	}

}
