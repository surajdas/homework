package package1;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Student  {
	private String name;
	private String gmail;
	private int id;

	public Student() {

	}

	public Student(String name, String gmail, int id) {
		this.name = name;
		this.gmail = gmail;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGmail() {
		return gmail;
	}

	public void setGmail(String gmail) {
		this.gmail = gmail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", gmail=" + gmail + ", id=" + id + "]";
	}
    

	 public void init() {
		 System.out.println("helo");
		 
	 }
	 public void destroy() {
		 System.out.println("bhaii");
	 }

}
