package package1;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
   public static void main(String[] args) {
	   ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
	   Student st1=context.getBean("s1",Student.class);
	   System.out.println(st1);
	    
	   st1=context.getBean("s2",Student.class);
	   System.out.println(st1);
	    
	   context.registerShutdownHook();
   }
}
