package pkg1;

import org.springframework.beans.factory.xml.XmlBeanFactory;
 import org.springframework.core.io.ClassPathResource;
public class Test1 {

	public static void main(String[] args) {
 XmlBeanFactory factory=new XmlBeanFactory (new ClassPathResource("AppContext.xml"));
	    Student st1=(PrimaryStudent)factory.getBean("prmStdnt");
	    st1.getInfo();
	    st1=( SecondryStudents)factory.getBean("secStdnt");
	    st1.getInfo();
	}

}
