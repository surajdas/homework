package pkg1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new 
				ClassPathXmlApplicationContext("AppContext.xml"); 
		Student s1=context.getBean("prmStdnt",PrimaryStudent.class);
		 s1.getInfo();
		s1=context.getBean("secStdnt",SecondryStudents.class);
	      s1.getInfo();
            
	}

}
