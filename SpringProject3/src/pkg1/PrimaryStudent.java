package pkg1;

public class PrimaryStudent implements Student{
	private int rollNo;
	private String fullName;
	
	
	public PrimaryStudent() {
		 this.rollNo=45;
		 this.fullName="suraj";
	}


	public PrimaryStudent(int rollNo, String fullName) {
		super();
		this.rollNo = rollNo;
		this.fullName = fullName;
	}
    
	public PrimaryStudent(String fullName,int rollNo) {
		this.fullName = fullName;
		this.rollNo = rollNo;
	}
    
	public PrimaryStudent(PrimaryStudent ps) {
		 
		this.rollNo = ps.rollNo;
		this.fullName = ps.fullName;
	}
    
	public int getRollNo() {
		return rollNo;
	}


	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	@Override
	public String toString() {
		return "PrimaryStudent [rollNo=" + rollNo + ", fullName=" + fullName + "]";
	}


	@Override
	public String getInfo() {
		 
		return("PrimaryStudent [rollNo=" + rollNo + ", fullName=" + fullName + "]");
	}
	
	
	

}
