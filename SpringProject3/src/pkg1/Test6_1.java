package pkg1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test6_1 {
      public static void main(String[] args) {
    	  ClassPathXmlApplicationContext context=new 
        		  ClassPathXmlApplicationContext("AppContext.xml");
          Student s1=context.getBean("std1",PrimaryStudent.class);
          System.out.println(s1);
          
          s1=context.getBean("std3",PrimaryStudent.class);
          System.out.println(s1);
          
          s1=context.getBean("std4",PrimaryStudent.class);
          System.out.println(s1);
          
          s1=context.getBean("std5",PrimaryStudent.class);
          System.out.println(s1);
          
          s1=context.getBean("std6",PrimaryStudent.class);
          System.out.println(s1);
          
          s1=context.getBean("std7",PrimaryStudent.class);
          System.out.println(s1);
          
          
          Student s2=context.getBean("std7",PrimaryStudent.class);
          Student s3=context.getBean("std7",PrimaryStudent.class);
          System.out.println(s2);
          System.out.println(s3);
	}
      
}
