package WebTime;

import java.text.DateFormat;
import java.util.Date;

 
public class webTimeBean {
	
	public String getTime()
	{
		return DateFormat.getTimeInstance(DateFormat.LONG).format(new Date());
	}

}
