package pkg2;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2 {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new 
				ClassPathXmlApplicationContext("AppContext.xml"); 
		
		PersonInf obj1=context.getBean("myStudent",Student.class);
		 obj1=context.getBean("myPerson",Person.class);
		System.out.println(obj1.get_Message());
		System.out.println(obj1.get_Message());
		context.close();
	}

}
