package FirstDayAug9;

import javax.swing.JOptionPane;

/**
 *
 * @author SURAJ BANIYA
 */
public class Addition {
 
    public static void main(String[] args) {
        //joptionpane create input/output dialog box
        String num1=JOptionPane.showInputDialog("Enter the First Number");
        String num2=JOptionPane.showInputDialog("Enter then Second Number");
        
        int num3=Integer.parseInt(num1);
        int num4=Integer.parseInt(num2);
        int sum=num3+num4;
        //diaplay the result into joptionpane message diaolog
        JOptionPane.showMessageDialog(null, "the sum of number "+num3 +"and"+ num4+" is:"+sum);
         
    }
    
}
