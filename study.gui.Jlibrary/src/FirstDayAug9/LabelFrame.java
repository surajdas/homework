 
package FirstDayAug9;

import java.awt.FlowLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author SURAJ BANIYA
 */
public class LabelFrame extends JFrame{
      
    private  JLabel label1;//jlabel with text
    private JLabel label2;// jlabel  constructed with text and icon
    private JLabel label3;//label with added text and icon
    
    public LabelFrame(){
        super("Testing Jlabel");
        setLayout(new FlowLayout());//set Frame layOut
        
        //label constructor with string argument
        label1=new JLabel("label with text");
        label1.setToolTipText(" this is label1");
        add(label1);//add label 1 to frame
        
        //jlabel with constructor with a string argument,icon, and aligment String
        Icon bug=new ImageIcon(getClass().getResource("bug1.png"));
        label2=new JLabel("label2 with text and icon",bug,SwingConstants.LEFT);
        label2.setToolTipText("this is label2");
        add(label2);
        
        label3=new JLabel();//label three with no argument
        label3.setText("label with icon and text at button");
        label3.setIcon(bug);//add icon to the jlabel
        label3.setHorizontalTextPosition(SwingConstants.CENTER);
        label3.setToolTipText("this is label3");
        add(label3);
        
        
        
    }
    
    
    
}
