/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SecondDayAug10;

/**
 *
 * @author SURAJ BANIYA
 * 
 */

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author SURAJ BANIYA
 */

public class NewMainWindow implements ActionListener {

    JFrame frame;
    JLabel lbl_1;
    JLabel lbl_2;
    JLabel lbl_3;
    JTextField txt_1;
    JTextField txt_2;
    JTextField txt_3;
    JButton btn_close;
    JButton btn_Add;
    JButton btn_sub;
    JButton btn_mult;
    JButton btn_pow;
    JButton btn_div;

    public NewMainWindow() {
        frame = new JFrame();
        lbl_1 = new JLabel();
        lbl_2 = new JLabel();
        lbl_3 = new JLabel();
        txt_1 = new JTextField();
        txt_2 = new JTextField();
        txt_3 = new JTextField();
        btn_close = new JButton();
        btn_Add = new JButton();
        btn_sub = new JButton();
        btn_mult = new JButton();
        btn_pow = new JButton();
        btn_div = new JButton();

        frame.setSize(350,200);
        frame.setTitle("CALCULATOR");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());

        lbl_1.setText("Enter Number: ");
        frame.add(lbl_1);
        txt_1.setColumns(20);
        frame.add(txt_1);

        lbl_2.setText("Enter Number: ");
        frame.add(lbl_2);
        txt_2.setColumns(20);
        frame.add(txt_2);

        btn_Add.setText("ADD");
        btn_Add.addActionListener(this);
        frame.add(btn_Add);

        btn_sub.setText("SUB");
        btn_sub.addActionListener(this);
        frame.add(btn_sub);

        btn_mult.setText("MULT");
        btn_mult.addActionListener(this);
        frame.add(btn_mult);

        btn_pow.setText("POW");
        btn_pow.addActionListener(this);
        frame.add(btn_pow);

        btn_div.setText("DIV");
        btn_div.addActionListener(this);
        frame.add(btn_div);

        btn_close.setText("CLOSE");
        btn_close.addActionListener(this);
        frame.add(btn_close);

        lbl_3.setText("OutPut");
        frame.add(lbl_3);
        txt_3.setColumns(20);
        frame.add(txt_3);

        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == btn_close) {
            System.exit(0);
        } 
        else if (ae.getSource() == btn_Add) {

            double n1, n2, Add;
            n1 = Double.parseDouble(txt_1.getText());
            n2 = Double.parseDouble(txt_2.getText());
          /*  CalculatorData c = new CalculatorData(n1, n2);
            Add = c.doAdd();
            txt_3.setText(Double.toString(Add));
                  */

        }
        if(ae.getSource()==btn_sub)
        {
            double num1,num2,subt;
            num1=Double.parseDouble(txt_1.getText());
            num2=Double.parseDouble(txt_2.getText());
         /*   CalculatorData c1=new CalculatorData(num1,num2);
            subt=c1.doSub();
            txt_3.setText(Double.toString(subt));
                 */
        }
        if(ae.getSource()==btn_mult)
        {
           double num1,num2,num3;
           num1=Double.parseDouble(txt_1.getText());
           num2=Double.parseDouble(txt_2.getText());
        /*   CalculatorData c=new CalculatorData(num1, num2);
           num3=c.doMul();
           txt_3.setText(Double.toString(num3));
                */
        }
        if(ae.getSource()==btn_div)
        {
           double number1,number2;
           number1=Double.parseDouble(txt_1.getText());
           number2=Double.parseDouble(txt_2.getText());
           double number;
         /*  CalculatorData c=new CalculatorData(number1, number2);
           number=c.doDiv();
           txt_3.setText(Double.toString(number));
           */
        }
        if(ae.getSource()==btn_pow)
        {
           double number1,number2;
           number1=Double.parseDouble(txt_1.getText());
           number2=Double.parseDouble(txt_2.getText());
           double number;
      /*     CalculatorData c=new CalculatorData(number1, number2);
           number=c.doPow();
           txt_3.setText(Double.toString(number));
           */
        }
    }

}

