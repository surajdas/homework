package pkg1;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Database {

	SessionFactory factory;
	Session session;
	boolean result = false;

	public Database() {
		factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();
		session = factory.getCurrentSession();
	}

	public boolean inserRecord(Student student) {
		try {
			session.beginTransaction();
			session.save(student);
			session.beginTransaction().commit();
			result = true;
			return result;

		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			result = false;
			return result;
		}
	}

	public boolean updateRecord(Student student) {
		try {
			session.beginTransaction();
			int id = student.getId();
			Student student1 = session.get(Student.class, id);
			student1.setName(student.getName());
			student1.setEmail(student.getEmail());
			student1.setAddress(student.getAddress());
			session.beginTransaction().commit();
			result = true;
			return result;

		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			result = false;
			return result;
		}
	}

	public boolean deleteRecord(Student student) {
		try {
			session.beginTransaction();
			int id = student.getId();
			Student student1 = session.get(Student.class, id);
			session.delete(student1);
			session.beginTransaction().commit();
			result = true;
			return result;
		} catch (Exception e) {
			result = false;
			return result;
		}
	}
	
	public Student searchREcord(Student student)
	{
		Student student1=null;
		try {
			session.beginTransaction();
			student1=session.get(Student.class,student.getId());
			session.beginTransaction().commit();
		} catch (Exception e) {
			 System.out.println("Error:"+e.getMessage());
		}
		return student1;
	}
	public List<Student> getAll(){
		 List<Student> student=new ArrayList<Student>();
		try {
			session.beginTransaction();
			student=session.createQuery("from Student").list();
			session.beginTransaction().commit();
			
		} catch (Exception e) {
			System.out.println("Error:"+e.getMessage());
		}
		return student;
	}

}
